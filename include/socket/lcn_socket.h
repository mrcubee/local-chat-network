#ifndef LCN_SOCKET_H
#define LCN_SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "libmy.h"

#define LCN_PORT 42
#define LCN_BUF 255

char *listen_broadcast(void);
int send_string(char *str);

#endif
