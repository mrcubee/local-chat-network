#ifndef LCN_H
#define LCN_H

#include "socket/lcn_socket.h"
#include "libmy.h"

typedef struct lcn_message_s {
    char *author;
    char *message;
} lcn_message;

#endif
