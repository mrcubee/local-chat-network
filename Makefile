##
## EPITECH PROJECT, 2018
## makefile
## File description:
## main makefile
##

SRC		=	src/main.c		\
			src/socket/listen.c	\
			src/socket/send.c	\

TESTS		=	##

OBJ		=	$(SRC:.c=.o)

CFLAGS		=	-Wall -Wextra -g3

CPPFLAGS	=	-I ./include/ -I ./lib/my/include

LIB_PATH	=	./lib/my/

LIB		=	-L./lib/my -lmy

TESTS_PATH	=	./tests/

NAME		=	lcn

CC		=	gcc

RM		=	rm -f

all: 		$(NAME)

$(NAME):	lib_make $(OBJ)
		$(CC) -o $(NAME) $(OBJ) $(LIB)

clean:
		$(RM) $(OBJ)

fclean:		lib_clean clean 
		$(RM) $(NAME)

lib_make:
		make --no-print-directory -C $(LIB_PATH)

lib_clean:
		make --no-print-directory fclean -C $(LIB_PATH)

test_run:
		make --no-print-directory -C $(TESTS_PATH)

clean_tests:
		make --no-print-directory -C fclean $(TESTS_PATH)

re:		fclean all
