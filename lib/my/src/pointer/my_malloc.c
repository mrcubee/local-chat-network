/*
** EPITECH PROJECT, 2018
** My-CLib
** File description:
** Malloc with init values
*/

#include "pointer/lib_pointer.h"

void *my_init_malloc(size_t number, size_t unit)
{
    size_t malloc_size = number * unit + unit;
    char *result;

    if (number < 1 || unit < 1)
        return (NULL);
    if ((result = malloc(malloc_size)) == NULL)
        return (NULL);
    for (size_t i = 0; i < malloc_size; i++)
        result[i] = 0;
    return (result);
}

void *my_malloc(size_t number, size_t unit)
{
    size_t malloc_size = number * unit + unit;
    char *result;

    if (number < 1 || unit < 1)
        return (NULL);
    if ((result = malloc(malloc_size)) == NULL)
        return (NULL);
    for (size_t i = malloc_size - unit; i < malloc_size; i++)
        result[i] = 0;
    return (result);
}
