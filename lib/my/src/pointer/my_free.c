/*
** EPITECH PROJECT, 2019
** My-CLib
** File description:
** Several useful function for free.
*/

#include "pointer/lib_pointer.h"

void my_free(void *pointer)
{
    if (pointer)
        free(pointer);
}

void my_double_free(void **pointer)
{
    if (!pointer)
        return;
    for (int i = 0; pointer[i]; i++)
        free(pointer[i]);
    free(pointer);
}
