/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** atoi
*/

#include "string/lib_string.h"

static long my_atoi(char *str)
{
    size_t str_length = my_strlen(str);
    unsigned long temp_pow;
    long result= 0;

    if (str_length < 1)
        return (0);
    temp_pow = my_pow(10, str_length - 1);
    for (size_t i = 0; str[i]; i++, temp_pow /=10)
        result += (str[i] - 48) * temp_pow;
    return (result);
}

static int is_nbr(char c)
{
    if (c < 48 || c > 57)
        return (0);
    return (1);
}

static long scan_backward(char const *str, int index, int nb_count, int negatif)
{
    int result = 0;
    char *str_nbr = my_malloc(nb_count, sizeof(char));

    if (index < 0 || !str_nbr)
        return (0);
    for (int i = 0; i < nb_count; i++)
        str_nbr[nb_count - i - 1] = str[index - i];
    result = my_atoi(str_nbr);
    free(str_nbr);
    if (negatif != 0)
        result *= -1;
    return (result);
}

long my_getnbr(char const *str)
{
    int i = 0;
    int nbr_c = 0;
    int neg_c = 0;
    int result = 0;

    for (; str && str[i] != '\0'; i++) {
        if (is_nbr(str[i]))
            nbr_c++;
        else if (nbr_c > 0) {
            result = scan_backward(str, i - 1, nbr_c, neg_c % 2);
            return (result);
        } else
            (str[i] == '-') ? neg_c++ : ((str[i] != '+') ? neg_c = 0 : neg_c);
    }
    return (scan_backward(str, i - 1, nbr_c, neg_c % 2));
}
