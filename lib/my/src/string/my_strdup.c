/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** clone string
*/

#include "string/lib_string.h"

char *my_strdup(char const *str)
{
    char *new_str = my_malloc(my_strlen(str), sizeof(char));

    if (!new_str)
        return (NULL);
    for (size_t i = 0; str[i]; i++)
        new_str[i] = str[i];
    return (new_str);
}
