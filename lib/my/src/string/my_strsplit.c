/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string split by char
*/

#include "string/lib_string.h"

struct split {
    char const *str;
    size_t str_len;
    char const *split;
    size_t split_len;
};

static int contain_word(struct split * const spt)
{
    if (!spt || spt->split_len > spt->str_len)
        return (0);
    for (size_t i = 0; i < spt->split_len; i++)
        if (spt->str[i] != spt->split[i])
            return (0);
    return (1);
}

static int count_split(struct split * const spt)
{
    size_t count = 0;
    struct split new_split;

    if (!spt)
        return (0);
    new_split.split = spt->split;
    new_split.split_len = spt->split_len;
    for (size_t i = 0; i < spt->str_len; i++) {
        new_split.str = spt->str + i;
        new_split.str_len = spt->str_len - i;
        if (contain_word(&new_split))
            count++;
    }
    return (++count);
}

static char *extract_str(struct split * const spt, size_t *new_index)
{
    struct split new_split;

    if (!spt || !new_index)
        return (NULL);
    new_split.split = spt->split;
    new_split.split_len = spt->split_len;
    for (size_t i = 0; i < spt->str_len; i++) {
        new_split.str = spt->str + i;
        new_split.str_len = spt->str_len - i;
        if (contain_word(&new_split)) {
            *new_index += i + spt->split_len;
            return (my_getstr(spt->str, 0, i - 1));
        }
    }
    return (my_getstr(spt->str, 0, --spt->str_len));
}

char **my_strsplit(char const *str, char const *split_str, size_t *length)
{
    size_t str_len = my_strlen(str);
    struct split split_info = {str, str_len, split_str, my_strlen(split_str)};
    size_t count = 0;
    char **result;
    size_t index = 0;

    if (length == NULL || split_info.str_len < 1 || split_info.split_len < 1)
        return (NULL);
    count = count_split(&split_info);
    result = my_malloc(count, sizeof(char *));
    if (result == NULL)
        return (NULL);
    *length = count;
    for (size_t i = 0; i < count; i++) {
        split_info.str = str + index;
        split_info.str_len = str_len - index;
        result[i] = extract_str(&split_info, &index);
    }
    return (result);
}

char **my_strsplit_clean(char const *str, char const *split)
{
    size_t length = 0;
    size_t new_length = 0;
    char **result;
    char **values = my_strsplit(str, split, &length);

    if (values == NULL)
        return (NULL);
    for (size_t i = 0; i < length; i++)
        if (values[i])
            new_length++;
    if ((result = my_malloc(new_length, sizeof(char *))) == NULL) {
        my_double_free((void **) values);
        return (NULL);
    }
    new_length = 0;
    for (size_t i = 0; i < length; i++)
        result[(values[i]) ? new_length++ : new_length] = values[i];
    free(values);
    return (result);
}
