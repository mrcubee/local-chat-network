/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** format string and printf
*/

#include "string/lib_string.h"

static char *replace(char const *format, va_list *ap, size_t i, size_t *adlen)
{
    char *replace = NULL;
    char *rst;

    switch (format[i + 1]) {
    case 's':
        replace = va_arg(*ap, char *);
        replace = my_strdup(replace);
        break;
    case 'd':
        replace = my_itoa(va_arg(*ap, int));
        break;
    case '%':
        replace = my_strdup("%");
        break;
    }
    if (adlen)
        *adlen += my_strlen(replace) - 2;
    rst = my_stredit(format, i, i + 1, replace);
    my_free(replace);
    return (rst);
}

static char *format_str(char const *format, va_list *ap)
{
    size_t format_len = my_strlen(format);
    size_t add_len = 0;
    char *temp;
    char *result = my_strdup(format);

    if (format_len < 1 || !ap)
        return (NULL);
    for (size_t i = 0; format && format[i]; i++) {
        if (format[i] == '%' && format[i + 1]) {
            temp = replace(result, ap, i + add_len, &add_len);
            my_free((!temp) ? NULL : result);
            result = (!temp) ? result : temp;
            i++;
        }
    }
    return (result);
}

char *my_format(char const *format, ...)
{
    va_list ap;
    char *result;

    if (!format)
        return (NULL);
    va_start(ap, format);
    result = format_str(format, &ap);
    va_end(ap);
    return (result);
}

void my_printf(char const *format, ...)
{
    va_list ap;
    char *result;

    if (!format)
        return;
    va_start(ap, format);
    result = format_str(format, &ap);
    va_end(ap);
    my_putstr(result);
    my_free(result);
}
