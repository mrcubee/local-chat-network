/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** edit string
*/

#include "string/lib_string.h"

static void swap(size_t *from, size_t *to)
{
    size_t temp;

    temp = *from;
    *from = *to;
    *to = temp;
}

char *my_stredit(char const *str, size_t from, size_t to, char *replace)
{
    size_t str_len;
    size_t length;
    size_t replace_length = 0;
    char *result;

    if (to < from)
        swap(&from, &to);
    if ((str_len = my_strlen(str)) < 1 || to > str_len)
        return (NULL);
    length = str_len + my_strlen(replace) - to + from - 1;
    if (!(result = my_malloc(length, sizeof(char))))
        return (NULL);
    for (size_t i = 0; i < from; i++)
        result[i] = str[i];
    for (; replace && replace[replace_length]; replace_length++)
        result[replace_length + from] = replace[replace_length];
    for (size_t i = to + 1; str[i]; i++)
        result[from + replace_length + i - to - 1] = str[i];
    return (result);
}
