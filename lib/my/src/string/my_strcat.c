/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string cat
*/

#include "string/lib_string.h"

char *my_strcat(char const *str1, char const *str2)
{
    int len_1 = my_strlen(str1);
    int len_2 = my_strlen(str2);
    int len_r = len_1 + len_2;
    char *result;

    if (len_r < 1 || (result = my_malloc(len_r, sizeof(char))) == NULL)
        return (NULL);
    for (int i = 0; i < len_1; i++)
        result[i] = str1[i];
    for (int i = len_1; i < len_r; i++)
        result[i] = str2[i - len_1];
    return (result);
}
