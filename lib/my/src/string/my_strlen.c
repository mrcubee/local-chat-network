/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** string length
*/

#include "string/lib_string.h"

size_t my_strlen(char const *str)
{
    size_t len = 0;

    for (; str && str[len]; len++);
    return (len);
}

size_t my_str_arraylen(char * const *array)
{
    size_t len = 0;

    for (; array && array[len]; len++);
    return (len);
}
