/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** string contains char
*/

#include "string/lib_string.h"

static int str_eq(char const *str1, char const *str2)
{
    for (int i = 0; str1[i] && str2[i]; i++)
        if (str1[i] != str2[i])
            return (0);
    return (1);
}

int my_strcontains(char const *contain, char const *str)
{
    size_t contain_len = my_strlen(contain);
    size_t str_len = my_strlen(str);

    if (contain_len < 1 || str_len < 1 || contain_len < str_len)
        return (0);
    for (int i = 0; contain[i] && contain_len - i >= str_len; i++)
        if (str_eq(contain + i, str))
            return (1);
    return (0);
}
