/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** print string
*/

#include "string/lib_string.h"

int my_putstr_error(char const *str)
{
    int len = my_strlen(str);

    if (len < 1)
        return (len);
    write(2, str, len);
    return (len);
}
