/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** string compare
*/

#include "string/lib_string.h"

int my_streq(char const *str1, char const *str2)
{
    int len1 = my_strlen(str1);
    int len2 = my_strlen(str2);

    if (len1 < 1 || len2 < 1 || len1 != len2)
        return (0);
    for (int i = 0; str1[i] && str2[i]; i++)
        if (str1[i] != str2[i])
            return (0);
    return (1);
}
