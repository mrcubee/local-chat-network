/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** extract string
*/

#include "string/lib_string.h"

static void swap(size_t *from, size_t *to)
{
    size_t temp;

    temp = *from;
    *from = *to;
    *to = temp;
}

char *my_getstr(char const *str, size_t from, size_t to)
{
    size_t str_length = my_strlen(str);
    size_t result_length;
    char *result;

    if (to < from)
        swap(&from, &to);
    if (str_length < 1 || to >= str_length)
        return (NULL);
    result_length = to - from + 1;
    if (!(result = my_malloc(result_length, sizeof(char))))
        return (NULL);
    for (size_t i = from; i <= to; i++)
        result[i - from] = str[i];
    return (result);
}
