/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string array to string
*/

#include "string/lib_string.h"

char *my_strats(char **array, char *separator)
{
    size_t array_len = 0;
    size_t str_len = 0;
    size_t separator_len = my_strlen(separator);
    size_t global_i = 0;
    char *str;

    for (; array && array[array_len]; array_len++)
        str_len += my_strlen(array[array_len]);
    if (!(str = my_malloc(str_len + (array_len * separator_len), sizeof(char))))
        return (NULL);
    for (size_t a = 0; a < array_len; a++) {
        for (size_t i = 0; a > 0 && i < separator_len; i++, global_i++)
            str[global_i] = separator[i];
        for (size_t i = 0; array[a][i]; i++, global_i++)
            str[global_i] = array[a][i];
    }
    return (str);
}
