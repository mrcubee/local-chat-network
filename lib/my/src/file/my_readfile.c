/*
** EPITECH PROJECT, 2018
** My CLIB
** File description:
** read file
*/

#include "file/lib_file.h"

char *my_readfile(char const *file_path)
{
    struct stat file_stat;
    int file_desc;
    int r_size;
    char *result;

    if (stat(file_path, &file_stat) < 0)
        return (NULL);
    result = my_malloc(file_stat.st_size, sizeof(char));
    if (result == NULL || (file_desc = open(file_path, O_RDONLY)) < 0)
        return (NULL);
    r_size = read(file_desc, result, file_stat.st_size);
    close(file_desc);
    if (r_size != file_stat.st_size) {
        free(result);
        return (NULL);
    }
    return (result);
}
