/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** number difference
*/

#include "integer/lib_integer.h"


static void swap(long *from, long *to)
{
    long temp;

    temp = *from;
    *from = *to;
    *to = temp;
}

long my_nbrdif(long from, long to)
{
    long result;

    if (from > to)
        swap(&from, &to);
    result = to - from;
    if (result < 0)
        result *= -1;
    return (result);
}
