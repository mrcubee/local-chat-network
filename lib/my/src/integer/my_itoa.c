/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** int to string
*/

#include "integer/lib_integer.h"

char *my_itoa(long nbr)
{
    int temp_digit;
    unsigned long temp_pow;
    int neg = (nbr < 0);
    int len = my_nbrlen(nbr);
    char *result = my_malloc(len, sizeof(char));

    if (result == NULL)
        return (NULL);
    if (neg) {
        nbr *= -1;
        result[0] = '-';
    }
    temp_pow = my_pow(10, (len -= neg) - 1);
    for (int i = 0; i < len; i++, temp_pow /= 10) {
        temp_digit = nbr / temp_pow;
        result[i + neg] = temp_digit + 48;
        nbr -= temp_digit * temp_pow;
    }
    return (result);
}
