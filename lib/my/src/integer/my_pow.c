/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** power number
*/

long my_pow(long nbr, int pow)
{
    long result = 1;

    for (int i = 0; i < pow; i++)
        result *= nbr;
    return (result);
}
