/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** number length
*/

int my_nbrlen(int nbr)
{
    int	len = 1;

    if (nbr < 0)
        len++;
    for (; (nbr /= 10) != 0; len++);
    return (len);
}
