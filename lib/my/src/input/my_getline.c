/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** get line in standard input
*/

#include "input/lib_input.h"

struct buffer_s {
    char *buffer;
    size_t length;
};

static struct buffer_s *buffer = NULL;

static int init_buffer(void)
{
    if (buffer)
        return (1);
    if (!(buffer = malloc(sizeof(struct buffer_s))))
        return (0);
    buffer->buffer = NULL;
    buffer->length = 0;
    return (1);
}

static char *extract_line(void)
{
    char *temp;
    char *result;

    for (size_t i = 0; buffer->buffer[i]; i++) {
        if (buffer->buffer[i] == '\n') {
            result = (i > 0) ? my_getstr(buffer->buffer, 0, i - 1) : NULL;
            temp = my_stredit(buffer->buffer, 0, i, NULL);
            my_free(buffer->buffer);
            buffer->buffer = temp;
            return (result);
        }
    }
    return (NULL);
}

static void add_to_buffer(char *read_buffer, ssize_t read_size)
{
    char *new_buffer;
    size_t size = buffer->length;
    size_t total_size = size + read_size;

    if (read_size < 1 || !(new_buffer = my_malloc(total_size, sizeof(char))))
        return;
    if (read_buffer[read_size - 1] == '\n')
        read_size -= 1;
    for (size_t i = 0; i < total_size; i++)
        new_buffer[i] = (i < size) ? buffer->buffer[i] : read_buffer[i - size];
    buffer->length += read_size;
    my_free(buffer->buffer);
    buffer->buffer = new_buffer;
}

size_t my_getline(char **lineptr, int fd)
{
    char *temp = NULL;
    ssize_t read_size = 1;
    char *line = NULL;

    if (!lineptr || !init_buffer())
        return (-1);
    while (read_size > 0 && !line) {
        temp = my_malloc(255, sizeof(char));
        read_size = (!temp) ? -1 : read(fd, temp, 255);
        add_to_buffer(temp, read_size);
        free(temp);
        line = extract_line();
    }
    if (read_size < 1 && line) {
        my_getline_clear();
        *lineptr = line;
        return (my_strlen(line));
    }
    *lineptr = (!line) ? buffer->buffer : line;
    return ((!line) ? my_strlen(buffer->buffer) : my_strlen(line));
}

void my_getline_clear(void)
{
    if (buffer) {
        my_free(buffer->buffer);
        free(buffer);
    }
    buffer = NULL;
}
