/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string streq unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_streq, bad_parameter_null_1)
{
    cr_assert(my_streq(NULL, "Test :-)") == 0);
}

Test(my_clib_string_streq, bad_parameter_null_2)
{
    cr_assert(my_streq("Ce Test :-)", NULL) == 0);
}

Test(my_clib_string_streq, str_not_equals_other_str)
{
    cr_assert(my_streq("Ce Test :-)", "Ce Test :-(") == 0);
}

Test(my_clib_string_streq, str_equals_other_str)
{
    cr_assert(my_streq("Ce Test :-)", "Ce Test :-)") == 1);
}
