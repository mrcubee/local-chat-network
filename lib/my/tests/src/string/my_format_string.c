/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string format unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_format, bad_parameter_null)
{
    char *result;

    cr_assert((result = my_format(NULL, 84, 0)) == NULL);
    my_free(result);
}

Test(my_clib_string_format, bad_parameter_without_char)
{
    char *result;

    cr_assert((result = my_format("\0", 84, 0)) == NULL);
    my_free(result);
}

Test(my_clib_string_format, format_str)
{
    char *format = "error = %d normal = %d";
    char *normal_rst = "error = 84 normal = 0";
    int error = 84;
    int normal = 0;
    char *result;

    cr_assert_str_eq((result = my_format(format, error, normal)), normal_rst);
    my_free(result);
}
