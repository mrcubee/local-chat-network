/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string strcat unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strcat, parameter_null_1)
{
    char *result;

    cr_assert_str_eq((result = my_strcat(NULL, "Coucou :-)")), "Coucou :-)");
    if (result != NULL)
        free(result);
}

Test(my_clib_string_strcat, parameter_null_2)
{
    char *result;

    cr_assert_str_eq((result = my_strcat("Coucou", NULL)), "Coucou");
    if (result != NULL)
        free(result);
}

Test(my_clib_string_strcat, str_cat)
{
    char *result;

    cr_assert_str_eq((result = my_strcat("Hello ", "World")), "Hello World");
    if (result != NULL)
        free(result);
}
