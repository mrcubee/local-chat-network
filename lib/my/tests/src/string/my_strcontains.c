/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string strcontains unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strcontains, bad_parameter_null_1)
{
    cr_assert(my_strcontains(NULL, "Test :-)") == 0);
}

Test(my_clib_string_strcontains, bad_parameter_null_2)
{
    cr_assert(my_strcontains("Ce Test :-) est Cool.", NULL) == 0);
}

Test(my_clib_string_strcontains, str_contains_other_str)
{
    cr_assert(my_strcontains("Ce Test :-) est Cool.", ":-)") == 1);
}

Test(my_clib_string_strcontains, str_not_contains_other_str)
{
    cr_assert(my_strcontains("Ce Test :-) est Cool.", ":-(") == 0);
}
