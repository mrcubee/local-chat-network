/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string stredit unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_stredit, bad_parameter_null)
{
    char *result;

    cr_assert((result = my_stredit(NULL, 0, 1, "Coucou :-)")) == NULL);
    if (result != NULL)
        free(result);
}

Test(my_clib_string_stredit, bad_parameter)
{
    char *result;

    cr_assert((result = my_stredit("Hello World", 12, 6, "Bot")) == NULL);
    if (result != NULL)
        free(result);
}

Test(my_clib_string_stredit, edit_str)
{
    char *str = "Hello World";
    char *result;

    cr_assert_str_eq((result = my_stredit(str, 6, 10, "Bot")), "Hello Bot");
    if (result != NULL)
        free(result);
}

Test(my_clib_string_stredit, cut_str)
{
    char *result;

    cr_assert_str_eq((result = my_stredit("Hello World", 0, 5, NULL)), "World");
    if (result != NULL)
        free(result);
}
