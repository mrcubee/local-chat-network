/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string getstr unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_getstr, bad_parameter_null)
{
    char *result;

    cr_assert((result = my_getstr(NULL, 0, 1)) == NULL);
    if (result != NULL)
        free(result);
}

Test(my_clib_string_getstr, bad_parameter)
{
    char *result;

    cr_assert((result = my_getstr("Hello World", 11, 0)) == NULL);
    if (result != NULL)
        free(result);
}

Test(my_clib_string_getstr, extract_str)
{
    char *result;

    cr_assert_str_eq((result = my_getstr("Hello World", 6, 10)), "World");
    if (result != NULL)
        free(result);
}
