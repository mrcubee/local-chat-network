/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string strsplit unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strsplit, bad_parameter_null)
{
    char **result;

    cr_assert((result = my_strsplit(NULL, NULL, NULL)) == NULL);
    if (result != NULL)
        my_double_free((void **) result);
}

Test(my_clib_string_strsplit, bad_parameter)
{
    size_t length = 0;
    char **result;

    cr_assert((result = my_strsplit(NULL, NULL, &length)) == NULL);
    cr_assert(length == 0);
    if (result != NULL)
        my_double_free((void **) result);
}

Test(my_clib_string_strsplit, split_str)
{
    size_t length = 0;
    char **result = my_strsplit("Hello  World", " ", &length);

    cr_assert(result != NULL);
    cr_assert(length == 3);
    cr_assert_str_eq(result[0], "Hello");
    cr_assert(result[1] == NULL);
    cr_assert_str_eq(result[2], "World");
    if (result != NULL)
        my_double_free((void **) result);
}

Test(my_clib_string_strsplit, split_str_remove_null)
{
    char **result = my_strsplit_clean("Hello  World", " ");

    cr_assert(result != NULL);
    cr_assert_str_eq(result[0], "Hello");
    cr_assert(result[1] != NULL);
    cr_assert_str_eq(result[1], "World");
    if (result != NULL)
        my_double_free((void **) result);
}
