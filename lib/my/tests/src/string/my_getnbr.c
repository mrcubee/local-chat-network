/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string getnbr unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_getnbr, bad_parameter_null)
{
    cr_assert(my_getnbr(NULL) == 0);
}

Test(my_clib_string_getnbr, without_number)
{
    cr_assert(my_getnbr("Coucou :-)") == 0);
}

Test(my_clib_string_getnbr, positive_number)
{
    cr_assert(my_getnbr("eliott0209") == 209);
}

Test(my_clib_string_getnbr, negative_number)
{
    cr_assert(my_getnbr("eliott-+-+---0209") == -209);
}
