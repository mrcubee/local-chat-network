/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string array to string unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strats, bad_parameter_null)
{
    char *result;

    cr_assert((result = my_strats(NULL, NULL)) == NULL);
    my_free(result);
}

Test(my_clib_string_strats, bad_parameter_null_2)
{
    char *array[2] = {NULL, NULL};
    char *result;

    cr_assert((result = my_strats(array, NULL)) == NULL);
    my_free(result);
}

Test(my_clib_string_strats, array_to_string)
{
    char **array = my_strsplit_clean("Coucou Eliott :-)", " ");
    char *result = my_strats(array, NULL);

    my_double_free((void **) array);
    cr_assert_str_eq(result, "CoucouEliott:-)");
    my_free(result);
}

Test(my_clib_string_strats, array_to_string_with_separator)
{
    char **array = my_strsplit_clean("Coucou Eliott :-) ça va ?", " ");
    char *result = my_strats(array, " ");

    my_double_free((void **) array);
    cr_assert_str_eq(result, "Coucou Eliott :-) ça va ?");
    my_free(result);
}
