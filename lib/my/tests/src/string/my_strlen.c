/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string strlen unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strlen, bad_parameter_null)
{
    cr_assert(my_strlen(NULL) == 0);
}

Test(my_clib_string_strlen, without_char)
{
    cr_assert(my_strlen("\0") == 0);
}

Test(my_clib_string_strlen, str_len)
{
    cr_assert(my_strlen("Hello World") == 11);
}

Test(my_clib_string_str_array_len, bad_parameter_null)
{
    cr_assert(my_str_arraylen(NULL) == 0);
}

Test(my_clib_string_str_array_len, without_str)
{
    char **array = my_malloc(1, sizeof(char *));

    array[0] = NULL;
    cr_assert(my_str_arraylen(array) == 0);
    my_double_free((void **) array);
}

Test(my_clib_string_str_array_len, array_len)
{
    char **array = my_malloc(3, sizeof(char *));

    array[0] = "Hello";
    array[1] = "World";
    array[2] = "!";
    cr_assert(my_str_arraylen(array) == 3);
    my_free(array);
}
