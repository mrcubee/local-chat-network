/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** string my_strdup unit tests
*/

#include "unit_tests.h"

Test(my_clib_string_strdup, bad_parameter_null)
{
    char *result = my_strdup(NULL);

    cr_assert(!result);
    my_free(result);
}

Test(my_clib_string_strdup, bad_parameter_without_char)
{
    char *result = my_strdup("\0");

    cr_assert(!result);
    my_free(result);
}

Test(my_clib_string_strdup, clone_str)
{
    char *str = "My CLib unit tests";
    char *clone = my_strdup(str);

    cr_assert_str_eq(str, clone);
}
