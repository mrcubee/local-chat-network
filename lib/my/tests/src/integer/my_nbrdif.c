/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** integer nbrdif unit tests
*/

#include "unit_tests.h"

Test(my_clib_integer_nbrdif, dif_number_1)
{
    cr_assert(my_nbrdif(3, 12) == 9);
}

Test(my_clib_integer_nbrdif, dif_number_2)
{
    cr_assert(my_nbrdif(1, -1) == 2);
}

Test(my_clib_integer_nbrdif, dif_number_3)
{
    cr_assert(my_nbrdif(-12, -3) == 9);
}
