/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** integer itoa unit tests
*/

#include "unit_tests.h"

Test(my_clib_integer_itoa, parameter_positive_number)
{
    char *result;

    cr_assert_str_eq((result = my_itoa(209)), "209");
    if (result != NULL)
        free(result);
}

Test(my_clib_integer_itoa, parameter_negative_number)
{
    char *result;

    cr_assert_str_eq((result = my_itoa(-209)), "-209");
    if (result != NULL)
        free(result);
}
