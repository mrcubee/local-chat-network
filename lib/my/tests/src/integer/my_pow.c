/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** integer my_pow unit tests
*/

#include "unit_tests.h"

Test(my_clib_integer_pow, pow_positive_number)
{
    cr_assert(my_pow(10, 2) == 100);
}

Test(my_clib_integer_pow, pow_negative_number)
{
    cr_assert(((int) my_pow(-10, 2)) == 100);
}

Test(my_clib_integer_pow, pow_number_zero)
{
    cr_assert(my_pow(0, 10) == 0);
}
