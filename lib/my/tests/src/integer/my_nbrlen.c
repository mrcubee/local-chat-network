/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** integer nbrlen unit tests
*/

#include "unit_tests.h"

Test(my_clib_integer_nbrlen, number_len_1)
{
    cr_assert(my_nbrlen(0) == 1);
}

Test(my_clib_integer_nbrlen, number_len_2)
{
    cr_assert(my_nbrlen(2) == 1);
}

Test(my_clib_integer_nbrlen, number_len_3)
{
    cr_assert(my_nbrlen(20) == 2);
}

Test(my_clib_integer_nbrlen, number_len_4)
{
    cr_assert(my_nbrlen(-20) == 3);
}
