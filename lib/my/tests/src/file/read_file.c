/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** file read_file unit tests
*/

#include "unit_tests.h"

Test(my_clib_file_read, file_not_exist)
{
    char *str = my_readfile("./toto_existe_pas.txt");

    cr_assert(str == NULL);
    my_free(str);
}
