/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** pointer malloc unit tests
*/

#include "unit_tests.h"

Test(my_clib_pointer_malloc, my_init_malloc)
{
    char *str = my_init_malloc(10, sizeof(char));

    for (int i = 0; i < 11; i++)
        cr_assert(str[i] == 0);
    free(str);
}

Test(my_clib_pointer_malloc, my_malloc)
{
    char *str = my_malloc(10, sizeof(char));

    cr_assert(str[10] == 0);
    free(str);
}
