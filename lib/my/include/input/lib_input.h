/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** lib file header
*/

#ifndef MY_CLIB_INPUT_H
#define MY_CLIB_INPUT_H

#include "pointer/lib_pointer.h"
#include "string/lib_string.h"

size_t my_getline(char **lineptr, int fd);
void my_getline_clear(void);

#endif
