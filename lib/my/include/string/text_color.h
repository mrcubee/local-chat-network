/*
** EPITECH PROJECT, 2019
** My CLib
** File description:
** text color code
*/

#ifndef MY_CLIB_STRING_COLOR_H
#define MY_CLIB_STRING_COLOR_H

#define FORMAT_RESET  '\033[0m'
#define FORMAT_BOLD '\033[1m'
#define FORMAT_RESET_BOLD '\033[21m'
#define FORMAT_DIM '\033[2m'
#define FORMAT_RESET_DIM '\033[22m'
#define FORMAT_UNDERLINED '\033[4m'
#define FORMAT_RESET_UNDERLINED '\033[24m'
#define FORMAT_BLINK '\033[5m'
#define FORMAT_RESET_BLINK '\033[25m'
#define FORMAT_REVERSE '\033[7m'
#define FORMAT_RESET_REVERSE '\033[27m'
#define FORMAT_HIDDEN '\033[8m'
#define FORMAT_RESET_HIDDEN '\033[28m'
#define COLOR_DEFAULT '\033[39m';
#define COLOR_BLACK '\033[30'
#define COLOR_RED '\033[31m'
#define COLOR_GREEN '\033[32m'
#define COLOR_YELLOW '\033[33m'
#define COLOR_BLUE '\033[34m'
#define COLOR_MAGENTA '\033[35m'
#define COLOR_CYAN '\033[36m'
#define COLOR_LIGHT_GRAY '\033[37m'
#define COLOR_DARK_GRAY '\033[90m'
#define COLOR_LIGHT_RED '\033[91m'
#define COLOR_LIGHT_GREEN '\033[92m'
#define COLOR_LIGHT_YELLOW '\033[93m'
#define COLOR_LIGHT_BLUE '\033[94m'
#define COLOR_LIGHT_MAGENTA '\033[95m'
#define COLOR_LIGHT_CYAN '\033[96m'
#define COLOR_LIGHT_WHITE '\033[97m'
#define BG_DEFAULT '\033[49m';
#define BG_BLACK '\033[40'
#define BG_RED '\033[41m'
#define BG_GREEN '\033[42m'
#define BG_YELLOW '\033[43m'
#define BG_BLUE '\033[44m'
#define BG_MAGENTA '\033[45m'
#define BG_CYAN '\033[46m'
#define BG_LIGHT_GRAY '\033[47m'
#define BG_DARK_GRAY '\033[100m'
#define	BG_LIGHT_RED	'\033[101m'
#define	BG_LIGHT_GREEN '\033[102m'
#define	BG_LIGHT_YELLOW '\033[103m'
#define	BG_LIGHT_BLUE '\033[104m'
#define	BG_LIGHT_MAGENTA '\033[105m'
#define	BG_LIGHT_CYAN '\033[106m'
#define	BG_LIGHT_WHITE '\033[107m'

#endif
