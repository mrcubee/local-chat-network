/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** lib string header
*/

#ifndef MY_CLIB_STRING_H
#define MY_CLIB_STRING_H

#include <unistd.h>
#include <stdarg.h>

#include "pointer/lib_pointer.h"
#include "integer/lib_integer.h"
#include "text_color.h"

size_t my_strlen(char const *str);
size_t my_str_arraylen(char * const *array);
char *my_strdup(char const *str);
int my_putstr(char const *str);
int my_putstr_error(char const *str);
int my_streq(char const *str1, char const *str2);
char *my_strcat(char const *str1, char const *str2);
char *my_getstr(char const *str, size_t from, size_t to);
char *my_stredit(char const *str, size_t from, size_t to, char *replace);
char *my_format(char const *format, ...);
void my_printf(char const *format, ...);
int my_strcontains(char const *contain, char const *str);
int my_getline_number(char const *str);
long my_getnbr(char const *str);
char **my_strsplit(char const *str, char const *split_str, size_t *length);
char **my_strsplit_clean(char const *str, char const *split);
char *my_strats(char **array, char *separator);

#endif
