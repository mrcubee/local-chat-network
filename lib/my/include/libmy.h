/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** Lib header
*/

#ifndef MY_CLIB_H
#define MY_CLIB_H

#include "input/lib_input.h"
#include "integer/lib_integer.h"
#include "pointer/lib_pointer.h"
#include "string/lib_string.h"
#include "file/lib_file.h"

#endif
