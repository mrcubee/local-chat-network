/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** My integer header
*/

#ifndef MY_CLIB_INTEGER_H
#define My_CLIB_INTEGER_H

#include <unistd.h>

#include "pointer/lib_pointer.h"

long my_pow(int nbr, int pow);
int my_nbrlen(int nbr);
char *my_itoa(long nbr);
long my_nbrdif(long from, long to);

#endif
