/*
** EPITECH PROJECT, 2018
** My CLIB
** File description:
** lib pointer header
*/

#ifndef MY_CLIB_POINTER_H
#define MY_CLIB_POINTER_H

#include <stdlib.h>

void *my_init_malloc(size_t number, size_t unit);
void *my_malloc(size_t number, size_t unit);
void my_free(void *pointer);
void my_double_free(void **pointer);

#endif
