/*
** EPITECH PROJECT, 2018
** My CLib
** File description:
** lib file header
*/

#ifndef MY_CLIB_FILE_H
#define MY_CLIB_FILE_H

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "pointer/lib_pointer.h"
#include "string/lib_string.h"

char *my_readfile(char const *file_path);

#endif
