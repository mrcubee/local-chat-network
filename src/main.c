#include "lcn.h"

int main(void)
{
    char *message;

    while (1) {
        if (my_strlen(message = listen_broadcast()) > 0)
            my_printf("message: %s\n", message);
        my_free(message);
    }
    return (0);
}
