#include "socket/lcn_socket.h"

/*
** Merge a string with a precise buffer size.
*/
static char *strcatbuffer(char *str, char *buffer, size_t buf_len)
{
    size_t str_len = my_strlen(str);
    size_t length = str_len + buf_len;
    char *result;

    if (!(result = my_malloc(length, sizeof(char))))
        return (NULL);
    for (size_t i = 0; i < length; i++)
        result[i] = (i < str_len) ? str[i] : buffer[i - str_len];
    return (result);
}

/*
** Retrieve a string of unknown size from a socket.
*/
static char *extract_string(int server_fd)
{
    char *result = NULL;
    char *temp = NULL;
    ssize_t read_len = 0;
    char buffer[LCN_BUF];

    while (!result || read_len > 0) {
        if ((read_len = recvfrom(server_fd, buffer, LCN_BUF, 0, NULL, 0)) == -1)
            return (NULL);
        else if (read_len > 0) {
            temp = strcatbuffer(result, buffer, read_len);
            my_free(result);
            result = temp;
        }
    }
    return (result);
}

/*
** Initialize listening on the local network
** to retrieve a string of unknown size.
*/
char *listen_broadcast(void)
{
    int server_fd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in server;

    if (server_fd == -1)
        return (NULL);
    server.sin_family = AF_INET;
    server.sin_addr.s_addr=INADDR_ANY;
    server.sin_port = htons(LCN_PORT);
    if (bind(server_fd, (struct sockaddr *) &server, sizeof(server)) == -1)
        return (NULL);
    return (extract_string(server_fd));
}
