#include "socket/lcn_socket.h"

/*
** Sends a character string
** to all devices connected to the local network.
*/
int send_string(char *str)
{
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in st;
    struct hostent *hp;
    size_t len = my_strlen(str);

    if (len == 0)
        return (0);
    if (fd == -1 || !(hp = gethostbyname("255.255.255.255")))
        return (-1);
    st.sin_family = AF_INET;
    st.sin_addr = *((struct in_addr *) hp->h_addr);
    st.sin_port = htons(LCN_PORT);
    if ((sendto(fd, str, len, 0, (struct sockaddr *) &st, sizeof(st))) == -1)
        return (-1);
    endhostent();
    return (1);
}
